#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/select.h>

int main()
{

	int gd[2],td[2],dt[2],hd[2],dh[2],th[2],dr[2],rd[2],fd9[2],gd0[2];
	int fd=0;char buf[10];

	if(pipe(gd)==-1 || pipe(td)==-1 || pipe(dt)==-1 || pipe(hd)==-1 || pipe(dh)==-1 || pipe(th)==-1 || pipe(dr)==-1 || pipe(rd)==-1)
	{
		printf("Pipe Failed\n" ); 
		return 1; 
	} 

	int t=0,h=0,end=20;
	int hare,turtle,god,rep;


	if((turtle=fork())==0)	//turtle
	{
		int t=0;
		while(1)
		{
			read(dt[0],&t,sizeof(int));
			t++;
			write(th[1],&t,sizeof(int));
			write(td[1],&t,sizeof(int));
		}
		exit(0);
	}

	write(dt[1],&t,sizeof(int));


	if((hare=fork())==0)	//hare
	{
		int h=0,t=0;
		const int MIN_DIST_FOR_SLEEP = 5;
		int sleep_counter = -1;
		int SLEEP_TIME = -1;
		while(1)
		{
			if(sleep_counter != -1) 
			{
			    	sleep_counter++;
				read(dh[0],&h,sizeof(int));
				read(th[0],&t,sizeof(int));
				write(hd[1],&h,sizeof(int));
				if(sleep_counter >= SLEEP_TIME) 
					sleep_counter = -1;
					continue;
			}
			read(dh[0],&h,sizeof(int));
			read(th[0],&t,sizeof(int));
			h+=2;
			write(hd[1],&h,sizeof(int));

			if(h-t >= 5)
			{
				srand(time(0));
				SLEEP_TIME = rand()%10;
				sleep_counter = 0;
			}
		}
		exit(0);
	}

	write(dh[1],&h,sizeof(int));

	
	if((rep=fork())==0)	//reporter
	{
		int t,h,sig=0;
		while(1)
		{
			read(dr[0],&t,sizeof(int));
			read(dr[0],&h,sizeof(int));
			printf("HARE AT:%d,TURTLE AT:%d\n",h,t);

			write(rd[1],&sig,sizeof(int));
			if(t>=20 || h>=20)
				break;
		}
		exit(0);
	}


	int w=0,sig=0;	//w=0 for turtle and w=1 for hare

	while(1)
	{

		read(td[0],&t,sizeof(int));
		read(hd[0],&h,sizeof(int));
		if(h<20 && t<20)
		{
			fd_set rfds;
			struct timeval tv;
			int retval;


			FD_ZERO(&rfds);
			FD_SET(0, &rfds);

			tv.tv_sec = 1;
			tv.tv_usec = 0;
			retval = select(1, &rfds, NULL, NULL, &tv);

			if (retval == -1)
				perror("select()");
			else if (retval != 0)
			{
				if((god=fork())==0)
				{
					int t,h;
					close(gd[0]);
					printf("Enter hare position:");
					scanf("%d",&h);
					printf("Enter turtle position:");
					scanf("%d",&t);
					write(gd[1],&t,sizeof(int));
					write(gd[1],&h,sizeof(int));
					exit(0);
				}
				wait(NULL);
				read(gd[0],&t,sizeof(int));
				read(gd[0],&h,sizeof(int));

			}
		}         

		if(h>=end)
		{
			w=1;
			write(dr[1],&t,sizeof(int));
			write(dr[1],&h,sizeof(int));

			//write to t and h
			write(dt[1],&t,sizeof(int));
			write(dh[1],&h,sizeof(int));

			read(rd[0],&sig,sizeof(int));
			break;
		}
		else if(t>=end)
		{
			write(dr[1],&t,sizeof(int));
			write(dr[1],&h,sizeof(int));

			//write to t and h
			write(dt[1],&t,sizeof(int));
			write(dh[1],&h,sizeof(int));

			read(rd[0],&sig,sizeof(int));
			break;
		}

		write(dr[1],&t,sizeof(int));
		write(dr[1],&h,sizeof(int));

		//write to t and h
		write(dt[1],&t,sizeof(int));
		write(dh[1],&h,sizeof(int));

		read(rd[0],&sig,sizeof(int));
	}

	//RESULT!!
	if(t==h)
		printf("Race Drawn\n");
	else
		printf("%s WON!!\n",(w==0)?"TURTLE":"HARE");

	kill(turtle,SIGTERM);
	kill(hare,SIGTERM);
	kill(rep,SIGTERM);
	kill(god,SIGTERM);

	return 0;
}

