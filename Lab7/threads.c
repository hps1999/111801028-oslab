#include <pthread.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <sys/types.h> 
#include <string.h> 
#include <sys/wait.h>
#include <time.h>
#include <sys/select.h>

int hare = 0;
int turtle = 0;

int hare_time = 0;
int turtle_time = 0;

const int TARGET = 200000;
pthread_t h,t,g,r;
pthread_mutex_t hlock; 
pthread_mutex_t ttlock; 
pthread_mutex_t tmlock; 

//TURTLE
void *turtle_turn(void *args) 
{
	while(turtle < TARGET) 
	{
		pthread_mutex_lock (&ttlock);
		turtle+=2;
		turtle_time++;
		pthread_mutex_unlock (&ttlock);
	}
	return NULL;

}

//HARE
void *hare_turn(void *args) {

while(hare < TARGET) 
{
	if(hare-turtle >=7500) 
	{
		srand(time(0));
		int tm=rand()%7500;
		hare_time += tm;
		usleep(tm);
	}
	pthread_mutex_lock (&hlock);
	hare += 5;
	hare_time++;
	pthread_mutex_unlock (&hlock);
}
return NULL;
}

//REPORTER
void *reporter_turn(void *args) 
{
	while(turtle < TARGET || hare < TARGET) 
	{
		pthread_mutex_lock (&ttlock);
		pthread_mutex_lock (&hlock);
		pthread_mutex_lock (&tmlock);
		printf("\nHARE AT:%d    \t\tIterations:%d\n",hare,hare_time);
		printf("TURTLE AT:%d    \t\tIterations:%d\n",turtle,turtle_time);
		pthread_mutex_unlock (&ttlock);
		pthread_mutex_unlock (&hlock);
		pthread_mutex_unlock (&tmlock);
		usleep(500);
	}
    return NULL;
}

//GOD
void *god_turn(void *args) 
{
	while(turtle < TARGET || hare < TARGET) 
	{
		pthread_mutex_lock (&ttlock);
		pthread_mutex_lock (&hlock);
		pthread_mutex_lock (&tmlock);

		fd_set rfds;
		struct timeval tv;
		int retval;

		FD_ZERO(&rfds);
		FD_SET(0, &rfds);

		tv.tv_sec = 1;
		tv.tv_usec = 0;
		retval = select(1, &rfds, NULL, NULL, &tv);

		if (retval == -1)
			perror("select()");
		else if (retval != 0)
		{
			if(hare < TARGET) 
			{
				printf("Enter new position of hare:");scanf("%d",&hare);
			}
			if(turtle < TARGET) 
			{
				printf("Enter new position of turtle:");scanf("%d",&turtle);
			}
		}
		else
		{
			pthread_mutex_unlock (&ttlock);
			pthread_mutex_unlock (&hlock);
			pthread_mutex_unlock (&tmlock);
			usleep(4000);
			continue;
		}
		pthread_mutex_unlock (&ttlock);
		pthread_mutex_unlock (&hlock);
		pthread_mutex_unlock (&tmlock);	    
    }
    return NULL;
}

int main() 
{
	srand(time(NULL));
	if (pthread_mutex_init(&hlock, NULL) != 0) 
	{ 
		printf("\n mutex init has failed\n"); 
		return 1; 
	} 
   	if (pthread_mutex_init(&ttlock, NULL) != 0) 
	{ 
		printf("\n mutex init has failed\n"); 
		return 1; 
	} 
	if (pthread_mutex_init(&tmlock, NULL) != 0) 
	{ 
		printf("\n mutex init has failed\n"); 
		return 1; 
	} 

	// Creating four threads
	pthread_create (&r, NULL, reporter_turn, NULL);
	pthread_create (&g, NULL, god_turn, NULL);

	pthread_create (&t, NULL, turtle_turn, NULL);
	pthread_create (&h, NULL, hare_turn, NULL);
    
	// Wait for each thread to finish their execution
	pthread_join (r, NULL);
	pthread_join (g, NULL);
	pthread_join (t, NULL);
	pthread_join (h, NULL);
   
	//RESULT!!!
	if (turtle_time < hare_time)
		printf("\nTURTLE WON\nNumber of Iterations = TURTLE : %d\tHARE : %d\n",turtle_time,hare_time);
	else if (turtle_time > hare_time)
		printf("\nHARE WON\nNumber of Iterations = HARE : %d\tTURTLE : %d\n",hare_time,turtle_time);
	else
		printf("\nRACE DRAWN :Number of Iterations = HARE : %d\tTURTLE : %d\n",hare_time,turtle_time);

	pthread_mutex_destroy(&hlock);
	pthread_mutex_destroy(&ttlock);
	pthread_mutex_destroy(&tmlock);
	return 0;
}
